class Api::V1::SessionsController < Devise::SessionsController
  skip_before_filter :verify_signed_out_user, only: :destroy

  # Copyright notice: This controller is based on this gist: https://gist.github.com/gonzalo-bulnes/9001010

  # This controller provides a JSON version of the Devise::SessionsController and
  # is compatible with the use of SimpleTokenAuthentication.
  # See https://github.com/gonzalo-bulnes/simple_token_authentication/issues/27

  def create
    # Fetch params
    email = params[:user][:email] if params[:user] && params[:user][:email]
    password = params[:user][:password] if params[:user] && params[:user][:password]

    id = User.find_by(email: email).try(:id) if email.presence

    if email.nil? or password.nil?
      render status: 400, json: {message: 'The request MUST contain the user email and password.'}
      return
    end

    # Authentication
    user = User.find_by(email: email)

    if user
      if user.valid_password? password
        user.restore_authentication_token!
        # Note that the data which should be returned depends heavily of the API client needs.
        render status: 200, json: {user: {email: user.email, authentication_token: user.authentication_token, id: id}}
      else
        render status: 401, json: {message: 'Invalid email or password.'}
      end
    else
      render status: 401, json: {message: 'Invalid email or password.'}
    end
  end

  def destroy
    # Fetch params
    user = User.find_by(authentication_token: params[:authentication_token])

    if user.nil?
      render status: 404, json: {message: 'Invalid token.'}
    else
      user.authentication_token = nil
      user.save!
      render status: 204, json: nil
    end
  end
end

