class Api::V1::UsersController < ApplicationController
  respond_to :json
  skip_before_filter :authenticate_user!, only: [:index, :show, :create]
  before_filter :find_user, only: [:show, :update, :destroy]
  before_filter :check_current_user, only: [:update, :destroy]
  before_filter :check_registration_parameters_present, only: [:create]

  def index
    @users = User.all
    render json: @users
  end

  def show
    render json: @user
  end

  def create
    @user = User.create(user_params)
    render json: @user, location: url_for([:api, :v1, @user])
  end

  def update
    @user.update_attributes(user_params)
    render json: @user
  end

  def destroy
    @user.destroy
    render json: @user
  end

  private

  def find_user
    begin
      @user = User.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      render status: 404, json: {message: 'User not found.'}
    end
  end

  def check_current_user
    render status: 401, json: {message: 'Invalid email or password.'} unless @user == current_user
  end

  def user_params
    params.require(:user).permit(
        :email,
        :password)
  end

  def check_registration_parameters_present
    begin
      params.require(:user).require('email')
    rescue ActionController::ParameterMissing
      render status: 401, json: {message: 'Email address missing.'}
    end
  end
end
