This project can be used as a scaffold for creating an own Rails API project. It uses the Devise gem for authentication
and simple token authentication for authenticating users. The database configuration template can be found in config/database.yml.dist
and is prepared to be used for Postgres databases. If another database shall be used, please make the appropiate changes.



Installation:
1.) Pull the project form the repository

2.) Copy config/databases.yml.dist to config/databases.yml and replace DBUSERNAME with the database user you want to use

3.) Copy config/secrets.yml.dist to config/secrets.yml.dist and choose secret keys with at least 30 characters as described there

4.) Install the dependencies:
bundle install

5.) Create the databases (please replace DBUSERNAME with the username you configured previously in databases.yml):
sudo -u postgres createuser -s DBUSERNAME
sudo -u postgres pqsl
\password DBUSERNAME

6.) Execute the database migrations:
bundle exec rake db:create
bundle exec rake db:migrate

7.) Start the server:
bundle exec rails server



Example usages (using curl):
Create a new user:
curl  -H "Accept: application/json" -H "Content-type: application/json" http://127.0.0.1:3000/api/v1/users -X POST -d '{"user":{"email": "foo@bar.com", "password": "password"}}'

Login:
curl  -H "Accept: application/json" -H "Content-type: application/json" http://127.0.0.1:3000/api/v1/login -X POST -d '{"user":{"email": "foo@bar.com", "password": "password"}}'

Logout (Please replace AUTHTOKEN with the user token you received while logging in the user):
curl  -H "Accept: application/json" -H "Content-type: application/json" http://127.0.0.1:3000/api/v1/logout -X DELETE -d '{":authentication_token": "AUTHTOKEN"}'

List all users:
curl  -H "Accept: application/json" -H "Content-type: application/json" http://127.0.0.1:3000/api/v1/users

Update a user (will generate an error message since no token is being provided):
curl  -H "Accept: application/json" -H "Content-type: application/json" http://127.0.0.1:3000/api/v1/users/1 -X PATCH -d '{"user":{"email": "test@example.com"}}'

Update a user (including token and email. Email is always required for the authentication, see https://github.com/gonzalo-bulnes/simple_token_authentication for reference):
ATTENTION! : This requestr changes the email address of the given user! So use the same email address for testing (so just replace the original email address with the same one).
curl  -H "Accept: application/json" -H "X-User-Email: USEREMAIL" -H "X-User-Token: DsxU3vs5F4CikHECRomt" -H "Content-type: application/json" http://127.0.0.1:3000/api/v1/users/USERID -X PATCH -d '{"user":{"email": "foo@bar.com"}}'

