describe 'Sessions API' do
  describe 'GET session' do
    it 'successfully creates a session for the given user' do
      user1 = FactoryGirl.create(:user)

      headers = {HTTP_ACCEPT: 'application/json'}

      post '/api/v1/login', {user: {email: user1.email, password: user1.password}}, headers

      expect(response.status).to eq 200

      json = JSON.parse(response.body)
      expect(json.length).to eq 1
      expect(json['user']['email']).to eq user1.email
      expect(json['user']['authentication_token']).to eq user1.authentication_token
    end

    it 'returns a 400 BAD REQUEST if no email and password are supplied' do
      headers = {HTTP_ACCEPT: 'application/json'}

      post '/api/v1/login', nil, headers

      expect(response.status).to eq 400
    end

    it 'returns a 401 UNAUTHORIZED if the email address is not registered' do
      headers = {HTTP_ACCEPT: 'application/json'}

      post '/api/v1/login', {user: {email: 'test', password:'test'}}, headers

      expect(response.status).to eq 401
    end

    it 'returns a 401 UNAUTHORIZED if the password is not valid' do
      user1 = FactoryGirl.create(:user)

      headers = {HTTP_ACCEPT: 'application/json'}

      post '/api/v1/login', {user: {email: user1.email, password: 'test'}}, headers

      expect(response.status).to eq 401
    end
  end

  describe 'DELETE session' do
    it 'successfully deletes the user session and return a 204 NO CONTENT' do
      user1 = create_and_sign_in_user

      headers = {HTTP_ACCEPT: 'application/json'}

      delete '/api/v1/logout', {authentication_token: user1.authentication_token}, headers

      expect(response.status).to eq 204
    end

    it 'returns a 404 NOT FOUND if the token is not assigned to a user' do
      headers = {HTTP_ACCEPT: 'application/json'}

      delete '/api/v1/logout', {authentication_token: 'test'}, headers

      expect(response.status).to eq 404
    end
  end
end
