describe 'Users API' do
  describe 'GET users' do
    it 'successfully gets all users' do
      user1 = FactoryGirl.create(:user)
      user2 = FactoryGirl.create(:user)

      get '/api/v1/users'

      expect(response.status).to eq 200
      json = JSON.parse(response.body)

      expect(json['users'].length).to eq 2
      expect(json['users'][0]['email']).to eq user1.email
      expect(json['users'][1]['email']).to eq user2.email
    end
  end

  describe 'SHOW user' do
    it 'successfully returns the first user in the database' do
      user1 = FactoryGirl.create(:user)
      FactoryGirl.create(:user)

      get "/api/v1/users/#{user1.id}"

      expect(response.status).to eq 200
      json = JSON.parse(response.body)

      expect(json.length).to eq 1
      expect(json['user']['email']).to eq user1.email
    end

    it 'returns a 404 NOT FOUND status code if the given user is not present' do
      get '/api/v1/users/1'

      expect(response.status).to eq 404
    end
  end

  describe 'CREATE user' do
    it 'successfully creates the user in the database' do
      user_to_create = {user: {email: 'test@example.com', password: 'password'}}
      post '/api/v1/users', user_to_create

      expect(response.status).to eq 200
      json = JSON.parse(response.body)

      expect(json.length).to eq 1
      expect(json['user']['email']).to eq user_to_create[:user][:email]
    end

    it 'returns a 401 UNAUTHORIZED if the email address is missing' do
      user_to_create = {user: {password: 'password'}}
      post '/api/v1/users', user_to_create

      expect(response.status).to eq 401
    end
  end

  describe 'UPDATE user' do
    it 'successfully updates the given user data if the user is logged in' do
      user1 = create_and_sign_in_user
      headers = {HTTP_ACCEPT: 'application/json',
                 'X-User-Email' => user1.email,
                 'X-User-Token' => user1.authentication_token}

      put "/api/v1/users/#{user1.id}", {user: {email: 'a@b.com'}}, headers

      expect(response.status).to eq 200

      json = JSON.parse(response.body)
      expect(json.length).to eq 1
      expect(json['user']['email']).to eq 'a@b.com'
    end

    it 'returns a 401 UNAUTHORIZED if the user is not logged in' do
      user1 = FactoryGirl.create(:user)
      put "/api/v1/users/#{user1.id}", nil, {HTTP_ACCEPT: 'application/json'}

      expect(response.status).to eq 401
    end

    it 'returns a 401 UNAUTHORIZED if the logged in user is not the user to be updated' do
      user1 = FactoryGirl.create(:user)
      user2 = FactoryGirl.create(:user)

      headers = {HTTP_ACCEPT: 'application/json',
                 'X-User-Email' => user1.email,
                 'X-User-Token' => user1.authentication_token}

      put "/api/v1/users/#{user2.id}", {user: {email: 'a@b.com'}}, headers

      expect(response.status).to eq 401
    end
  end

  describe 'DELETE user' do
    it 'successfully updates the given user data if the user is logged in' do
      user1 = create_and_sign_in_user
      headers = {HTTP_ACCEPT: 'application/json',
                 'X-User-Email' => user1.email,
                 'X-User-Token' => user1.authentication_token}

      delete "/api/v1/users/#{user1.id}", nil, headers

      expect(response.status).to eq 200

      json = JSON.parse(response.body)
      expect(json.length).to eq 1
      expect(json['user']['email']).to eq user1.email
    end

    it 'returns a 401 UNAUTHORIZED if the user is not logged in' do
      user1 = FactoryGirl.create(:user)
      delete "/api/v1/users/#{user1.id}", nil, {HTTP_ACCEPT: 'application/json'}

      expect(response.status).to eq 401
    end

    it 'returns a 401 UNAUTHORIZED if the logged in user is not the user to be updated' do
      user1 = FactoryGirl.create(:user)
      user2 = FactoryGirl.create(:user)

      headers = {HTTP_ACCEPT: 'application/json',
                 'X-User-Email' => user1.email,
                 'X-User-Token' => user1.authentication_token}

      delete "/api/v1/users/#{user2.id}", nil, headers

      expect(response.status).to eq 401
    end
  end
end
