# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
    sequence(:email) { |n| "person_#{n}@example.com"}
    password "secret123"
    password_confirmation "secret123"
    sequence(:authentication_token) { |n| "token_#{n}"}
  end
end
