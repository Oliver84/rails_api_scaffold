module AuthenticationHelper
  def login(user)
    post '/api/v1/login', {user: {email: user.email, password: user.password}}, {'HTTP_ACCEPT' => 'application/json'}
  end

  def create_and_sign_in_user
    user = FactoryGirl.create(:user)
    login(user)
    return user
  end

  alias_method :create_and_sign_in_another_user, :create_and_sign_in_user

  def create_and_sign_in_admin
    admin = FactoryGirl.create(:admin)
    login(admin)
    return admin
  end
end
